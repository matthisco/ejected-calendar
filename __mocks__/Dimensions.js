// Dimensions.js inside __mocks__ folder
"use strict";
const Fontsize = {
  get: jest.fn().mockReturnValue({width: 100, height:100})
}
module.exports = Fontsize