const isIphoneX = {
	get: jest.fn().mockReturnValue(true)
};
module.exports = isIphoneX;
