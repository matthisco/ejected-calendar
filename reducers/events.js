import * as helpers from "../helpers";
import moment from "moment";
import { eventColour } from "../data/eventType.js";

export function filterEvents(state = [], action) {
    switch (action.type) {
        case "FILTER_EVENTS":
            const events = action.data;
            const selectedId = action.id;

            const results = Object.values(events).filter(o =>
                o.dots.some(oo => oo.categories.some(c => c.id === selectedId))
            );

            return {
                results
            };

        default:
            return state;
    }
}

export function fetchEvents(state = {}, action) {
    switch (action.type) {
        case "EVENTS_SUCCESS":
            const events = {};
            const eventProps = {
                dots: [],
                disabled: false,
                selected: true,
                selectedColor: "#00CCCB",
                customStyles: {
                    text: {
                        marginTop: 3
                    }
                }
            };
            const filteredEvents = action.data.sort(
                (a, b) => a.startTimeString - b.startTimeString
            );

            filteredEvents.map(event => {
                const {
                    start: { dateTime: startTime },
                    end: { dateTime: endTime },
                    summary,
                    recurrence,
                    id,
                    description = ''
                } = event;

                // get span dates
                let allDayStart = event.start.date
                    ? moment(event.start.date)
                    : null;

                let allDayEnd = event.end.date ? moment(event.end.date) : null;

                const start = moment(startTime).format("YYYY-MM-DD HH:mm:ss");

                const end = moment(endTime).format("YYYY-MM-DD HH:mm:ss");

                const startDate = moment(startTime).format("YYYY-MM-DD");

                const currentTime =
                    moment().format("YYYY-MM-DD") + " " + "00:00:00";

                const allDayEvent = allDayEnd > allDayStart ? 1 : 0 || 0;

                const newEvent = {
                    summary,
                    start,
                    end,
                    allDayEvent,
                    id,
                    description
                };

                if (allDayStart) {
                    if (allDayStart.diff(allDayEnd, "days") == -1) {
                        allDayStart, allDayEnd = allDayStart.format("YYYY-MM-DD");
                    }

                    for (
                        var m = moment(allDayStart);
                        m.isSameOrBefore(allDayEnd);
                        m.add(1, "days")
                    ) {
                        const event = {
                            ...newEvent,
                            start: allDayStart,
                            end: allDayEnd
                        };

                        if (
                            m.isSameOrAfter(currentTime) &&
                            m.day() != 6 &&
                            m.day() != 7
                        ) {
                            if (events[m.format("YYYY-MM-DD")]) {
                                events[m.format("YYYY-MM-DD")].dots.push(event);
                            } else {
                                events[m.format("YYYY-MM-DD")] = {
                                    dots: [],
                                    disabled: false,
                                    selected: true,
                                    selectedColor: "#00CCCB",
                                    customStyles: {
                                        text: {
                                            marginTop: 3
                                        }
                                    }
                                };
                                events[m.format("YYYY-MM-DD")].dots.push(event);
                            }
                        }
                    }
                } else {
                    if (events[startDate]) {
                        events[startDate].dots.push(newEvent);
                    } else {
                        events[startDate] = {
                            dots: [],
                            disabled: false,
                            selected: true,
                            selectedColor: "#00CCCB",
                            customStyles: {
                                text: {
                                    marginTop: 3
                                }
                            }
                        };
                        events[startDate].dots.push(newEvent);
                    }
                }
            });

            return {
                ...events
            };
        case "EVENTS_FETCH_ERROR":
            return state;
        case "REFRESH_DATA":
            return { state, fetchEvents: {} };
        case "CLEAR_CREDENTIALS":
            return {};
        default:
            return state;
    }
}

export function eventsForMonth(state = {}, action) {
    switch (action.type) {
        case "EVENTS_AGENDA_MONTH": {
            const { data: events, day } = action;
            const newItems = {};

            for (let i = -15; i < 50; i++) {
                const strTime = helpers.timeToString(day, i);

                if (events[strTime]) {
                    newItems[strTime] = [...events[strTime].dots];
                } else {
                    newItems[strTime] = [];
                }
            }

            return {
                ...newItems
            };
        }

        case "EVENTS_CALENDAR_MONTH": {
            const { data, day } = action;
            const newItems = {};

            for (let i = -15; i < 60; i++) {
                const strTime = helpers.timeToString(day[0].timestamp, i);

                if (data[strTime]) {
                    newItems[strTime] = { ...data[strTime] };
                }
            }
            return {
                ...state,
                ...newItems
            };
        }
        default:
            return state;
    }
}

function sortName(a, b) {
    const textA =
        a.name.replace(/\D/g, "") != ""
            ? Number(a.name.replace(/\D/g, ""))
            : a.name.toUpperCase();

    const textB =
        b.name.replace(/\D/g, "") != ""
            ? Number(b.name.replace(/\D/g, ""))
            : b.name.toUpperCase();
    return textA < textB ? -1 : textA > textB ? 1 : 0;
}

function hasNumber(myString) {
    return /\d/.test(myString);
}

export function fetchNames(state = [], action) {
    switch (action.type) {
        case "CATEGORY_SUCCESS":
            const yearCategories = action.data.filter(
                category => category.parent == 0
            );
            const categorys = yearCategories.map(
                ({ id, name: label, parent }) => ({
                    id,
                    label,
                    parent,
                    options: action.data
                        .filter(({ parent }) => parent === id)
                        .sort(sortName)
                        .map(({ id, name: label, parent }) => ({
                            id,
                            parent,
                            label,
                            options: action.data
                                .filter(({ parent }) => parent === id)
                                .sort(sortName)
                                .map(({ id, name: label, parent }) => ({
                                    id,
                                    label,
                                    parent
                                }))
                        }))
                })
            );
            return [...categorys];

        default:
            return state;
    }
}

function checkEmptyArray(args) {
    const { group, data, year } = args;

    let findOptions = data
        .find(element => {
            return element.id == year;
        })
        .options.find(element => {
            return element.id == group;
        });

    let hasOption =
        findOptions.options && findOptions.options.length > 0 ? true : false;
    return hasOption;
}

export function changeMonth(state = false, action) {
    switch (action.type) {
        case "CHANGE_MONTH_NAME":
            return action.month;

        default:
            return state;
    }
}

export function authenticated(state = false, action) {
    switch (action.type) {
        case "SET_AUTH":
            return action.authentication;

        default:
            return state;
    }
}

const setCredentialsState = {
    showStudent: false,
    student: 0
};
export function setCredentials(state = setCredentialsState, action) {
    switch (action.type) {
        case "SET_YEAR":
            return {
                ...state,
                year: action.year
            };
        case "SET_STUDENT":
            return { ...state, student: action.id, label: action.label };
        case "SET_GROUP":
            const showStudent = checkEmptyArray(action);
            return {
                ...state,
                group: action.group,
                showStudent,
                label: action.label
            };
        case "SET_CREDENTIALS":
            const { data } = action;
            return { ...state, ...data };
        case "CLEAR_CREDENTIALS":
            return setCredentialsState;

        default:
            return state;
    }
}
