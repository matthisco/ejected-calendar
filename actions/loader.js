export const hasErrored = bool => ({
	type: "HAS_ERRORED",
	hasErrored: bool
});

export const isLoading = bool => ({
	type: "IS_LOADING",
	isLoading: bool
});

export const isLoadingCredentials = bool => ({
	type: "IS_LOADING_CREDENTIALS",
	isLoadingCredentials: bool
});

export const fetchSuccessCategories = data => ({
	type: "CATEGORY_SUCCESS",
	data
});

export const fetchSuccessEvents = data => ({
	type: "EVENTS_SUCCESS",
	data
});

export const fetchErrorEvents = data => ({
	type: "EVENTS_FETCH_ERROR",
	data
});
