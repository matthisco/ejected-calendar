export const key = "AIzaSyBkAEZGoQwB0f-Kmq4mCCzm93422up8oQw";
export const data = {
	years: [
		{
			label: "Year 3",
			id: 1,
			options: [
				{
					label: "Firm 01",
					id: "thirdyear@rguc.co.uk"
				},
				{
					label: "Firm 02",
					id:
						"rguc.co.uk_gdi0hs8dpirc05eh3hv9doboms@group.calendar.google.com"
				},
				{
					label: "Firm 03",
					id:
						"rguc.co.uk_d2ilim739aai1eoa8e3543e77s@group.calendar.google.com"
				},
				{
					label: "Firm 04",
					id:
						"rguc.co.uk_ome3khq0at9dfkhlgleca4q4n8@group.calendar.google.com"
				},
				{
					label: "Firm 05",
					id:
						"rguc.co.uk_nm7p4hn025mgiam1m1j3mvs6uc@group.calendar.google.com"
				},
				{
					label: "Firm 06",
					id:
						"rguc.co.uk_qm8rr8j0rf0e9um6r3jjdti26o@group.calendar.google.com"
				},
				{
					label: "Firm 07",
					id:
						"rguc.co.uk_3ggqvm0ojftusmbk287m2iqib0@group.calendar.google.com"
				},
				{
					label: "Firm 08",
					id:
						"rguc.co.uk_5hbugn8f7a4j3g7p6im1r1s1co@group.calendar.google.com"
				},
				{
					label: "Firm 09",
					id:
						"rguc.co.uk_afvauktrq31va6mv5mimfgsekc@group.calendar.google.com"
				},
				{
					label: "Firm 10",
					id:
						"rguc.co.uk_j90csckm1crq8lehug0fsk6p28@group.calendar.google.com"
				},
				{
					label: "Firm 11",
					id:
						"rguc.co.uk_fbfe6klltue92uqflsk2jkt4mg@group.calendar.google.com"
				},
				{
					label: "Firm 12",
					id:
						"rguc.co.uk_jh03b29taf6jso9ln5vd42lq60@group.calendar.google.com"
				},
				{
					label: "Firm 13",
					id:
						"rguc.co.uk_h3232j4npmv96ebh2uphebjrkk@group.calendar.google.com"
				},
				{
					label: "Firm 14",
					id:
						"rguc.co.uk_hn2a8ifnmlg9orjddlcl2e5tb4@group.calendar.google.com"
				},
				{
					label: "Firm 15",
					id:
						"rguc.co.uk_3lema0ha486sa617kq32e4mqkg@group.calendar.google.com"
				}
			]
		},
		{
			label: "Year 4",
			id: 2,
			options: [
				{
					label: "SPC",
					id: 4,
					options: [
						{
							label: "Student 01",
							id: "fourthyear-spc@rguc.co.uk"
						},
						{
							label: "Student 02",
							id:
								"rguc.co.uk_ecmtvie9jm8hnnn7rbtin6gjfc@group.calendar.google.com"
						},
						{
							label: "Student 03",
							id:
								"rguc.co.uk_dmctp28bfpie7f9crqo2u94ep8@group.calendar.google.com"
						},
						{
							label: "Student 04",
							id:
								"rguc.co.uk_us7lvjunv10g1o4kfh2lfqj608@group.calendar.google.com"
						},
						{
							label: "Student 05",
							id:
								"rguc.co.uk_r3bc94vpbqtjdb0n0jl6kn2pp8@group.calendar.google.com"
						},
						{
							label: "Student 06",

							id:
								"rguc.co.uk_6ou865ql84lp4qon0f2oh67u3s@group.calendar.google.com"
						},
						{
							label: "Student 07",

							id:
								"rguc.co.uk_terval4ck18920tcoaj0i43co8@group.calendar.google.com"
						},
						{
							label: "Student 08",

							id:
								"rguc.co.uk_3irb50gjq28g4n8c06prnt26p4@group.calendar.google.com"
						},
						{
							label: "Student 09",

							id:
								"rguc.co.uk_372udj3q8hk0vht6o95lu4hu50@group.calendar.google.com"
						},
						{
							label: "Student 10",

							id:
								"rguc.co.uk_t96c553o6prth8lbe288pnt4dc@group.calendar.google.com"
						},
						{
							label: "Student 11",

							id:
								"rguc.co.uk_hrj9n3ml14nk7uvtaipq4lgi8s@group.calendar.google.com"
						},
						{
							label: "Student 12",

							id:
								"rguc.co.uk_v13qsl1bfkvq85as1b0gff0k8c@group.calendar.google.com"
						},
						{
							label: "Student 13",

							id:
								"rguc.co.uk_o0d6sar462e4bafnmmgvreq4rg@group.calendar.google.com"
						},
						{
							label: "Student 14",

							id:
								"rguc.co.uk_dqkll6nbvfmfb28puro5hlo9ds@group.calendar.google.com"
						}
					]
				},
				{
					label: "SPM Group A",
					id: 5,
					options: [
						{
							label: "Student 01",
							id: "fourthyear-spma@rguc.co.uk"
						},
						{
							label: "Student 02",
							id:
								"rguc.co.uk_tufm1kkovuoj95ii6lb2pvj478@group.calendar.google.com"
						},
						{
							label: "Student 03",
							id:
								"rguc.co.uk_u6io9ov5op4m3a4l92r2dtl09o@group.calendar.google.com"
						},
						{
							label: "Student 04",
							id:
								"rguc.co.uk_5nenjmde53cn2jrpunsgpsa914@group.calendar.google.com"
						},
						{
							label: "Student 05",
							id:
								"rguc.co.uk_da11ele6eiu4aicqidi9mu0moo@group.calendar.google.com"
						},
						{
							label: "Student 06",
							id:
								"rguc.co.uk_m18trpctgg3jbvo2622am1touo@group.calendar.google.com"
						},
						{
							label: "Student 07",
							id:
								"rguc.co.uk_9tb76ee90mnnpo9oo977tdkhpc@group.calendar.google.com"
						},
						{
							label: "Student 08",
							id:
								"rguc.co.uk_m9nf55knbf9ji1t4l454gktrvk@group.calendar.google.com"
						},
						{
							label: "Student 09",
							id:
								"rguc.co.uk_a3hfh8k1jor7b6kd54t6kpbogs@group.calendar.google.com"
						},
						{
							label: "Student 10",
							id:
								"rguc.co.uk_bs5f1mjpkcptk3hcp6o09t063s@group.calendar.google.com"
						},
						{
							label: "Student 11",
							id:
								"rguc.co.uk_918qkngikjc7g84nbi7p9ma1sk@group.calendar.google.com"
						},
						{
							label: "Student 12",
							id:
								"rguc.co.uk_l5prmiqhqoo3dgg3nrchsugvp8@group.calendar.google.com"
						},
						{
							label: "Student 13",
							id:
								"rguc.co.uk_8sapebja6qdco321mt54fkhqno@group.calendar.google.com"
						},
						{
							label: "Student 14",
							id:
								"rguc.co.uk_8lgm4a32fb59orqqsm36fnvs5g@group.calendar.google.com"
						},
						{
							label: "Student 15",
							id:
								"rguc.co.uk_9lqn5cp0nae1d1hsa23s5ivtfc@group.calendar.google.com"
						},
						{
							label: "Student 16",
							id:
								"rguc.co.uk_tg3lfb6rf0fitf3udsvo25emu0@group.calendar.google.com"
						},
						{
							label: "Student 17",
							id:
								"rguc.co.uk_7it4vmou8e9gch4fmj80e3j5n4@group.calendar.google.com"
						}
					]
				},
				{
					label: "SPM Group B",
					id: 6,
					options: [
						{
							label: "Student 01",
							id: "fourthyear-spmb@rguc.co.uk"
						},
						{
							label: "Student 02",
							id:
								"rguc.co.uk_norrgc61sdk990m02mtlr4keis@group.calendar.google.com"
						},
						{
							label: "Student 03",
							id:
								"rguc.co.uk_itntm2kho7i4rkhufneo9osn1k@group.calendar.google.com"
						},
						{
							label: "Student 04",
							id:
								"rguc.co.uk_47j4jvg8ul1o6nf1icb4jes3tc@group.calendar.google.com"
						},
						{
							label: "Student 05",
							id:
								"rguc.co.uk_t99kgc2pq3o8mcrq5nga4u6l9c@group.calendar.google.com"
						},
						{
							label: "Student 06",
							id:
								"rguc.co.uk_j7hhm8b0gicb9jgqg72rgbsnds@group.calendar.google.com"
						},
						{
							label: "Student 07",
							id:
								"rguc.co.uk_k4adcbcddmvosbbjn43r441bao@group.calendar.google.com"
						},
						{
							label: "Student 08",
							id:
								"rguc.co.uk_bet5qt62hd0dm0v4usbcduvals@group.calendar.google.com"
						},
						{
							label: "Student 09",
							id:
								"rguc.co.uk_g1eu3k6ai25vmg9fo4c7svpuig@group.calendar.google.com"
						},
						{
							label: "Student 10",
							id:
								"rguc.co.uk_423d2kt46suk87gbutj2kb9nnk@group.calendar.google.com"
						},
						{
							label: "Student 11",
							id:
								"rguc.co.uk_i9so5nblu4qihdf4mt874nsjnc@group.calendar.google.com"
						},
						{
							label: "Student 12",
							id:
								"rguc.co.uk_j9f7neue3buochlkj5rvkjfcp8@group.calendar.google.com"
						},
						{
							label: "Student 13",
							id:
								"rguc.co.uk_j29k3bfrq9q95tvcsn7uipv9fg@group.calendar.google.com"
						},
						{
							label: "Student 14",
							id:
								"rguc.co.uk_8fb4al3cl00qmh4er8jcn3ta90@group.calendar.google.com"
						},
						{
							label: "Student 15",
							id:
								"rguc.co.uk_l54d17ld1gb5rnr2oo5en8rgpk@group.calendar.google.com"
						}
					]
				}
			]
		},
		{
			label: "Year 5",
			id: 3,
			options: [
				{
					label: "Group A",
					id: "fifthyear@rguc.co.uk",
					options: []
				},
				{
					label: "Group B",
					id:
						"rguc.co.uk_3vf7h0s6lerl5ei7aeaoh65aso@group.calendar.google.com",
					options: []
				}
			]
		}
	]
};
