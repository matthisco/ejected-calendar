![GitHub Logo](/img/logo.png)

Calendar app for students of the Ron Grimley Undergraduate Center

Android and iOS compatible.

## Try it out

```
$ npm install
$ react-native run-android / react-native run-ios
```

![GitHub Logo](/walkthrough.gif)
