import React, { Component } from "react";

import {
  StyleSheet,
  ActionSheetIOS,
  View,
  Text,
  TouchableOpacity
} from "react-native";

const PickerList = props => {
  const {
    label,
    options,
    selectedValue,
    name,
    onChange,
    styles,
    identifier
  } = props;

  const optionList = options.map(option => option.label);
  const selectedOption = options.find(option => option.id == selectedValue);

  const onSelectCategory = () => {
    ActionSheetIOS.showActionSheetWithOptions({ options: optionList, tintColor: '#006666', }, function(
      option
    ) {
      onChange(options[option].id);
    });
  };

  return (
    <TouchableOpacity onPress={onSelectCategory}>
      <Text style={[styles, { backgroundColor: "white", padding: 10, color: '#006666' }]}>
        {selectedOption ? selectedOption.label : `Please choose a ${label}`}
      </Text>
    </TouchableOpacity>
  );
};

export default PickerList;
