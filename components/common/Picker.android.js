import React, { Component } from "react";

import { Picker, Dimensions } from "react-native";

const PickerList = props => {
  const {
    label,
    options,
    selectedValue,
    name,
    onChange,
    identifier,
    style
  } = props;

  return (
    <Picker
      selectedValue={selectedValue}
      onValueChange={onChange}
      style={style}
    >
      <Picker.Item
        key={"unselectable"}
        label={"Please select an option"}
        value=""
      />
      {options.map((option, i) => {
        return (
          <Picker.Item key={option.id} label={option.label} value={option.id} />
        );
      })}
    </Picker>
  );
};

export default PickerList;
