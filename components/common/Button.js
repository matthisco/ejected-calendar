import React, { Component } from "react";
import PropTypes from "prop-types";

import { StyleSheet, Text, View, TouchableHighlight } from "react-native";

const Button = props => {
  const { onClick } = props;
  return (
    <TouchableHighlight
      onPress={onClick}
      style={
        this.state.landscape
          ? {
              alignItems: "center",
              backgroundColor: "#006666",
              padding: 10,
              width: Platform.OS === "ios" ? "100%" : "90%",
              alignSelf: "center"
            }
          : {
              alignItems: "center",
              backgroundColor: "#006666",
              padding: 10,
              width: Platform.OS === "ios" ? "100%" : "90%",
              alignSelf: "center"
            }
      }
    >
      <Text>hi </Text>
    </TouchableHighlight>
  );
};

export default Button;
