import React, { Component } from "react";

import { ImageBackground, View, ActivityIndicator } from "react-native";

const SpinnerWithBackground = () => {
  return (
    <ImageBackground
      source={require("../../img/bkgPhoto.jpg")}
      style={{
        flex: 1,
        width: null,
        height: null
      }}
    >
      <View
        style={{
          flex: 1,
          alignSelf: "center",
          justifyContent: "center"
        }}
      >
        <ActivityIndicator size="large" color="#67999A" />
      </View>
    </ImageBackground>
  );
};

export default SpinnerWithBackground;
