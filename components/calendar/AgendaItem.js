import React from "react";
import propTypes from "prop-types";
import { Text, View, WebView, Alert, TouchableHighlight } from "react-native";
import { filterString } from "../../helpers";
import moment from "moment";
import Icon from "react-native-vector-icons/FontAwesome";
import RF from "react-native-responsive-fontsize";
class AgendaItem extends React.PureComponent {
	constructor(props) {
		super(props);
		this._onPressButton = this._onPressButton.bind(this);
	}

	_onPressButton() {
		const { description } = this.props;
		Alert.alert("Info", description);
	}

	render() {
		const { start, end, summary, description } = this.props;
		let summaryNoHtml = summary.replace(/<\/?[^>]+(>|$)/g, "");
		return (
			<TouchableHighlight
				disabled={description == ""}
				onPress={this._onPressButton}
				style={{
					flex: 1,
					padding: 5,
					borderRadius: 5,
					backgroundColor: "#006666"
				}}
			>
				<View>
					{summary ? (
						<Text
							style={{
								color: "white",
								fontSize: RF(1.8)
							}}
						>
							{summaryNoHtml}
						</Text>
					) : null}
					{start ? (
						<Text
							style={{
								color: "white",

								fontSize: RF(1.8)
							}}
						>
							{moment(start).format("HH:mm") +
								" - " +
								moment(end).format("HH:mm")}
						</Text>
					) : null}

					{description ? (
						<View
							style={{
								flex: 1,
								justifyContent: "flex-end",
								marginBottom: 0
							}}
						>
							<Icon name="align-justify" size={18} color="#fff" />
						</View>
					) : null}
				</View>
			</TouchableHighlight>
		);
	}
}

AgendaItem.propTypes = {
	title: propTypes.string,
	venue: propTypes.string,
	colour: propTypes.string,
	organiser: propTypes.string,
	description: propTypes.string,
	start: propTypes.string,
	end: propTypes.string,
	allDay: propTypes.string,
	textColor: propTypes.string
};

export default AgendaItem;
