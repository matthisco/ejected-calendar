export default {
	studentYears: [
		{
			id: 0,
			title: "Year 1",
			value: 1,
			options: ["Firm 1", "Firm 2", "Firm 3"]
		},
		{
			id: 1,
			title: "Year 2",
			value: 2,
			options: ["Firm 2", "Firm 3", "Firm 4"]
		},
		{
			id: 2,
			title: "Year 3",
			value: 3,
			options: ["Firm 5", "Firm 6", "Firm 7"]
		}
	]
};
