import React, { Component } from "react";

import { createStackNavigator } from "react-navigation";
import HomeScreen from "../components/HomeScreen";
import Day from "../components/calendar/Agenda";
import Month from "../components/calendar/Month";

import CustomHeader from "./CustomHeader";
const AppNavigator = createStackNavigator(
	{
		Home: {
			screen: HomeScreen,
		},
		Month: {
			screen: Month,
			navigationOptions: () => ({
				header: props => <CustomHeader {...props} />
			})
		},
		Day: {
			screen: Day,
			navigationOptions: () => ({
				header: props => <CustomHeader {...props} />
			})
		}
	},
	{
		headerMode: "screen"
	}
);

export default AppNavigator;
