import React, { Component } from "react";

import { connect } from "react-redux";
import {
  Image,
  View,
  Text,
  Modal,
  Button,
  TouchableOpacity,
  AsyncStorage,
  StyleSheet,
  Platform,
  Alert,
  TouchableHighlight
} from "react-native";

import Icon from "react-native-vector-icons/FontAwesome";
import { NavigationActions } from "react-navigation";
import {
  setYear,
  setStudent,
  setGroup,
  fetchCategories,
  resetForm,
  resetData,
  fetchEvents
} from "../actions/events";

import { hasErrored } from "../actions/loader";

class CustomHeader extends Component {
  constructor() {
    super();

    this.resetForm = this.resetForm.bind(this);
    this.fetchEvents = this.fetchEvents.bind(this);
    this.showAlert = this.showAlert.bind(this);
    this.navigateToMonth = this.navigateToMonth.bind(this);
  }

  navigateToMonth() {
    const {
      navigation: { navigate }
    } = this.props;

    navigate("Month");
  }

  resetForm() {
    const {
      navigation: { navigate },
      resetForm
    } = this.props;
    AsyncStorage.removeItem("loggedIn", resetForm());

    navigate("Home");
  }

  showAlert() {
    Alert.alert("Events refreshed");
  }

  fetchEvents() {
    const {
      fetchEvents,
      navigation: { navigate },
      credentials: { student, group }
    } = this.props;
    resetData();
    fetchEvents(student || group);
    navigate("Month");
    this.showAlert();
  }

  render() {
    const {
      credentials: { label },
      monthName,
      navigation,
      navigation: {
        state: { index }
      }
    } = this.props;

    const { state } = navigation;
    const currentRouteKey = state.routes[state.index].routeName;

    return (
      <View style={styles.container}>
        {currentRouteKey == "Month" && (
          <TouchableOpacity onPress={this.resetForm}>
            <Icon name="sign-out" size={25} color="#fff" />
          </TouchableOpacity>
        )}

        {currentRouteKey == "Day" && (
          <TouchableOpacity onPress={this.navigateToMonth}>
            <Icon name="calendar" size={25} color="#fff" />
          </TouchableOpacity>
        )}

        <View style={styles.centerContainer}>
          <Text style={styles.text}>{label}</Text>
          <Text style={styles.text}>{monthName}</Text>
        </View>
        <TouchableOpacity onPress={this.fetchEvents}>
          <Image
            source={require("../img/refresh.png")}
            style={{ width: 20, height: 30 }}
          />
        </TouchableOpacity>
      </View>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    resetForm: () => dispatch(resetForm()),
    fetchEvents: id => dispatch(fetchEvents(id)),
    resetData: () => dispatch(resetData())
  };
};

const mapStateToProps = state => {
  return {
    categories: state.fetchCategories,
    monthName: state.changeMonth,
    isLoading: state.isLoading,
    credentials: state.setCredentials
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CustomHeader);

const styles = StyleSheet.create({
  container: {
    backgroundColor: "#006666",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    padding: 10,
    paddingLeft: 30,
    paddingRight: 30
  },
  centerContainer: {
    flexDirection: "column",
    alignItems: "center"
    // padding: 10,
    // paddingLeft: 30,
    // paddingRight: 30
  },
  text: {
    color: "#fff",
    fontSize: 14
  },
  button: {
    borderRadius: 0,
    color: "#fff"
  },
  buttonInvert: {
    borderRadius: 0,
    backgroundColor: "#fff",
    color: "#67999A",
    margin: 5
  }
});
