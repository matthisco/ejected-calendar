import React, { Component } from "react";
import { StyleSheet } from "react-native";

import { Provider } from "react-redux";
import store from "./config/configureStore";

import AppNavigation from "./navigation";
import { fetchNames } from "./actions/events";



export default class App extends React.Component {
	render() {
		return (
			<Provider store={store}>
				<AppNavigation />
			</Provider>
		);
	}
}
